Untitled
================
DanielH
October 21, 2018

-   [intro](#intro)
-   [data processing](#data-processing)
-   [data exploration](#data-exploration)
-   [typical budgets over time](#typical-budgets-over-time)

source: <https://www.youtube.com/watch?time_continue=1215&v=3-DRwg9yeNA>

intro
-----

This weeks case is about college majors. Our data is taken from the fivethirtyeight website.

We take a look at at the vars in the dataset

    ## [1] "X1"                "release_date"      "movie"            
    ## [4] "production_budget" "domestic_gross"    "worldwide_gross"  
    ## [7] "distributor"       "mpaa_rating"       "genre"

data processing
---------------

we do some data wrangling and create a new dataset: `movies_data_processed`

``` r
movies_data <-
  movie_dat_raw %>%
  select(-1) %>% 
  mutate(release_date = mdy(release_date),
         worldwide_profit = worldwide_gross / production_budget) %>%
  select(movie, distributor, genre,  release_date, mpaa_rating, everything()) %>%
  dmap_at(3, str_to_lower) %>%
  dmap_at(c(2:3, 5), factor)
```

We now check for duplicated entries

``` r
# get the index fo the duplicated entry
movies_data %>%
  duplicated() %>%
  detect_index(~. == TRUE)
```

    ## [1] 2975

``` r
# get the corresponding row in the dataframe
movies_data %>%
  slice(2975)
```

    ## # A tibble: 1 x 9
    ##   movie distributor genre release_date mpaa_rating production_budg~
    ##   <chr> <fct>       <fct> <date>       <fct>                  <dbl>
    ## 1 Tau ~ <NA>        acti~ 2010-04-02   R                    4000000
    ## # ... with 3 more variables: domestic_gross <dbl>, worldwide_gross <dbl>,
    ## #   worldwide_profit <dbl>

``` r
# check whether there actually are duplicates with that movie name
movies_data %>%
 dplyr::filter(str_detect(movie, "Tau ming"))
```

    ## # A tibble: 2 x 9
    ##   movie distributor genre release_date mpaa_rating production_budg~
    ##   <chr> <fct>       <fct> <date>       <fct>                  <dbl>
    ## 1 Tau ~ <NA>        acti~ 2010-04-02   R                    4000000
    ## 2 Tau ~ <NA>        acti~ 2010-04-02   R                    4000000
    ## # ... with 3 more variables: domestic_gross <dbl>, worldwide_gross <dbl>,
    ## #   worldwide_profit <dbl>

As we can see there's a duplicate entry for the movie: *Tau ming chong*: Thus we remove one row

``` r
# remove double entry
movies_data <-
  movies_data %>%
  dplyr::filter(!duplicated(movies_data))
  
# check
movies_data %>%
  duplicated() %>%
  keep(~. == TRUE) 
```

    ## logical(0)

``` r
# create decade variable and store new df in new var
movies_data <-
  movies_data %>%
  mutate(decade = 10 * floor(year(release_date) / 10),
         decade = factor(decade)) %>%
  select(movie:genre, decade, everything())

# processed data summary
movies_data %>%
  summary()
```

    ##     movie                       distributor         genre     
    ##  Length:3400        Warner Bros.      : 374   action   : 572  
    ##  Class :character   Sony Pictures     : 339   adventure: 481  
    ##  Mode  :character   Universal         : 307   comedy   : 813  
    ##                     20th Century Fox  : 282   drama    :1236  
    ##                     Paramount Pictures: 267   horror   : 298  
    ##                     (Other)           :1784                   
    ##                     NA's              :  47                   
    ##      decade      release_date        mpaa_rating  production_budget  
    ##  2000   :1397   Min.   :1936-02-05   G    :  85   Min.   :   250000  
    ##  2010   :1070   1st Qu.:1999-07-02   PG   : 573   1st Qu.:  9000000  
    ##  1990   : 607   Median :2005-09-29   PG-13:1092   Median : 20000000  
    ##  1980   : 229   Mean   :2004-02-04   R    :1513   Mean   : 33293356  
    ##  1970   :  64   3rd Qu.:2011-07-09   NA's : 137   3rd Qu.: 45000000  
    ##  1960   :  20   Max.   :2019-03-15                Max.   :175000000  
    ##  (Other):  13                                                        
    ##  domestic_gross      worldwide_gross     worldwide_profit  
    ##  Min.   :        0   Min.   :0.000e+00   Min.   :  0.0000  
    ##  1st Qu.:  6124348   1st Qu.:1.062e+07   1st Qu.:  0.7701  
    ##  Median : 25534156   Median :4.016e+07   Median :  1.9534  
    ##  Mean   : 45435115   Mean   :9.413e+07   Mean   :  4.2648  
    ##  3rd Qu.: 60357124   3rd Qu.:1.177e+08   3rd Qu.:  3.9368  
    ##  Max.   :474544677   Max.   :1.305e+09   Max.   :431.5179  
    ## 

data exploration
----------------

Since we have data for a long time, it might be a good idea to group it by decade. Thus we create a new variable `decade`

``` r
# plot
movies_data %>%  
  count(decade) %>%
  ggplot(aes(decade, n)) +
  geom_bar(stat = "identity", fill = "cadetblue") +
  ylim(0, 1500) +
  labs(title = "Movies produced each decade",
       subtitle = "total movies, 1930-2010",
       x = "", y = "")
```

<img src="CODE_week30_files/figure-markdown_github/unnamed-chunk-5-1.png" style="display: block; margin: auto;" />

Apparently the 2000 decade would appear to be the one with the highest movies produced BUT we have to remember that we only have 46 movies from january 2018 on and the period ends december 31, 2020. Nevertheless we cans safely say that the number of movies has been growing over time and the overwhelming majority of movies has been produced in the last 3-4 decades.

Since we have a lot fo different distributors we can lump some of them using the *fct\_lump()* function, as we've done before.

#### production budgets

``` r
movies_data %>%
  mutate(distributor = fct_lump(distributor, n = 6)) %>%
  dplyr::filter(!distributor == "NA") %>%  
  ggplot(aes(fct_reorder(genre, production_budget), production_budget, fill = genre)) +
  geom_boxplot(show.legend = F) +
  scale_y_log10(labels = comma_format()) +
  scale_fill_tableau(palette = "Tableau 10") +
  coord_flip() +
  facet_wrap(~distributor) +
  labs(title = "Production budget by genre for Majors",
       subtitle = "",
       x = "", y = "") +
  theme(plot.title = element_text(size = 28,
                                  family = "Times",
                                  face = "bold",
                                  color = "black",
                                  hjust = 0.5,
                                  vjust = 2,
                                  lineheight = 2),
        strip.background = element_rect(fill = "azure2"),
        strip.text =  element_text(size = 10),
        plot.subtitle = element_text(size = 11,
                                     hjust = 0,
                                     face = "italic"),
        axis.text.y = element_text(size = 10))
```

<img src="CODE_week30_files/figure-markdown_github/unnamed-chunk-6-1.png" style="display: block; margin: auto;" />

#### worldwide gross by genre

``` r
movies_data %>%
  mutate(distributor = fct_lump(distributor, n = 6)) %>%
  dplyr::filter(!distributor == "NA") %>%  
  ggplot(aes(fct_reorder(genre, worldwide_gross), worldwide_gross, fill = genre)) +
  geom_boxplot(show.legend = F) +
  scale_y_log10(labels = comma_format()) +
  scale_fill_tableau(palette = "Tableau 10") +
  coord_flip() +
  facet_wrap(~distributor) +
  labs(title = "Worldwide gross revenue by genre for Majors",
       subtitle = "adventure and action movies command highest revenues",
       x = "", y = "") +
  theme(plot.title = element_text(size = 21,
                                  family = "Times",
                                  face = "bold",
                                  color = "black",
                                  hjust = 0.5,
                                  vjust = 2,
                                  lineheight = 2),
        strip.background = element_rect(fill = "cadetblue4"),
        strip.text =  element_text(size = 11,
                                   color = "white",
                                   face = "bold"),
        plot.subtitle = element_text(size = 14,
                                     hjust = 0.5,
                                     face = "italic"),
        axis.text.y = element_text(size = 10))
```

<img src="CODE_week30_files/figure-markdown_github/unnamed-chunk-7-1.png" style="display: block; margin: auto;" />

The Aventure and action genres appear to be grossing the most in very case. Drama, on th other hand appears to generate the least revenues. We also notice that in the category "Other" the negative outliers appear to be much more frequent than in the rest of categories. It's likely that independet lables tend to produce movies which generate very low revenues

#### profit ratio

``` r
movies_data %>%
  mutate(distributor = fct_lump(distributor, n = 6)) %>%
  dplyr::filter(!distributor == "NA") %>%  
  ggplot(aes(fct_reorder(genre, worldwide_profit), worldwide_profit, fill = genre)) +
  geom_boxplot(show.legend = F) +
  #scale_y_log10(labels = comma_format()) +
  scale_fill_tableau(palette = "Tableau 10") +
  coord_flip() +
  facet_wrap(~distributor, scales = "free_x") +
  labs(title = "Worldwide profit ratio by genre for Majors",
       subtitle = "Horror movies have the highest profit ratios",
       x = "", y = "") +
  theme(plot.title = element_text(size = 21,
                                  family = "Times",
                                  face = "bold",
                                  color = "black",
                                  hjust = 0.5,
                                  vjust = 2,
                                  lineheight = 2),
        strip.background = element_rect(fill = "cadetblue4"),
        strip.text =  element_text(size = 11,
                                   color = "white",
                                   face = "bold"),
        plot.subtitle = element_text(size = 14,
                                     hjust = 0.5,
                                     face = "italic"),
        axis.text.y = element_text(size = 10))
```

<img src="CODE_week30_files/figure-markdown_github/unnamed-chunk-8-1.png" style="display: block; margin: auto;" />

typical budgets over time
-------------------------

Here we want to look at the typical budget over time.

``` r
movies_data %>%
  dplyr::filter(release_date > "1990-01-01") %>%
  group_by(year(release_date)) %>%
  summarize(median_budget = median(production_budget, na.rm = T)) %>%
  rename(year = 1) %>%
  ggplot(aes(year, median_budget)) +
  geom_point() +
  geom_line() +
  xlim(1990, 2020) +
  scale_y_continuous(labels = dollar_format()) +
  labs(title = "Typical budgets over time",
       subtitle = "median production budget 1990-2018",
       x = "", y = "")
```

![](CODE_week30_files/figure-markdown_github/unnamed-chunk-9-1.png)

There's something clearly supicious here in the value for year 2019, which definitely looks like an outlier

Let's check it out

``` r
movies_data %>%
  mutate(year = year(release_date)) %>% 
  dplyr::filter(year == 2019) %>% 
  select(movie, year, production_budget) %>%
  mutate(production_budget = parse_double(production_budget))
```

    ## # A tibble: 1 x 3
    ##   movie        year production_budget
    ##   <chr>       <dbl>             <dbl>
    ## 1 Wonder Park  2019         100000000

As we can see for the year 2019 we have only one movie, thati is and one value for the variable production\_budget thus the yearly median ends up being exactly that value. Definitely not a "typical" value.

The movie is *Wonder Park*. Of course, if we want to look at the typical production budgets over time we should remove it from our dataset.

Let's what happens if we remove it:

``` r
movies_data %>%
  mutate(year = year(release_date)) %>% 
  dplyr::filter(release_date > "1990-01-01",
                year != 2019) %>%
  group_by(year(release_date)) %>%
  summarize(median_budget = median(production_budget, na.rm = T)) %>%
  rename(year = 1) %>%
  ggplot(aes(year, median_budget)) +
  geom_point() +
  geom_line() +
  scale_y_continuous(labels = dollar_format()) +
  xlim(1990, 2020) +
  labs(title = "Typical budgets over time",
       subtitle = "median production budget, 1990-2018",
       x = "", y = "")
```

![](CODE_week30_files/figure-markdown_github/unnamed-chunk-11-1.png)

As we can see now we have a much more likely range of values.

#### profits by genre

We can use axis transformation in the case of profits. After doing that the plot becomes more readable

``` r
movies_data %>%
  ggplot(aes(fct_reorder(genre, worldwide_profit), worldwide_profit, fill = genre)) +
  geom_boxplot(show.legend = F) +
  scale_y_continuous(labels = comma_format()) +
  coord_flip() +
  labs(title = "Worldwide Profit by genre",
       x = "", y = "")
```

![](CODE_week30_files/figure-markdown_github/unnamed-chunk-12-1.png)

``` r
movies_data %>%
  ggplot(aes(fct_reorder(genre, worldwide_profit), worldwide_profit, fill = genre)) +
  geom_boxplot(show.legend = F) +
  scale_fill_tableau(palette = "Tableau 10") +
  scale_y_log10(labels = comma_format()) +
  coord_flip() +
  labs(title = "Worldwide Profit by genre, log-scale",
       x = "", y = "")
```

![](CODE_week30_files/figure-markdown_github/unnamed-chunk-12-2.png)

Now, we can group all three metrics in a column named `metric` and plot them on a single graph. In other words we want to go from wide format to long and plot

``` r
movies_data %>%
  mutate(year = year(release_date)) %>% 
  dplyr::filter(year > 1980) %>%
  group_by(decade) %>%
  summarise_at(c(7:9), median, na.rm = TRUE) %>% 
  gather(metric, value, - decade) %>%  # from wide to long
  ggplot(aes(decade, value, group = metric, color = metric)) +
  geom_line(size = 0.86) +
  geom_point(size = 1.66) +
  scale_color_manual(values = c("darkorange", "azure4", "firebrick1")) +
  scale_y_continuous(labels = dollar_format()) +
  labs(title = "More and more revenues are coming from abroad",
       subtitle = "median values by decade",
       x = "", y = "") +
  theme(legend.position = "top",
        legend.title = element_blank(),
        plot.title = element_text(size = 18,
                                  face = "bold",
                                  hjust = .0),
        plot.subtitle = element_text(size = 14,
                                     face = "italic",
                                     hjust = .5)) 
```

![](CODE_week30_files/figure-markdown_github/unnamed-chunk-13-1.png)

``` r
c("darkorange", "azure4", "firebrick1", "brown", "green")
```

    ## [1] "darkorange" "azure4"     "firebrick1" "brown"      "green"

``` r
movies_data %>%
  group_by(decade) %>%
  summarise_at(vars(production_budget:worldwide_gross), mean, na.rm = TRUE)
```

    ## # A tibble: 9 x 4
    ##   decade production_budget domestic_gross worldwide_gross
    ##   <fct>              <dbl>          <dbl>           <dbl>
    ## 1 1930            2700000       99421858.      195345120.
    ## 2 1940            1841562.      51039875        93382852 
    ## 3 1950            5245714.      37700000        37700460 
    ## 4 1960            9110350       33392352.       57900138.
    ## 5 1970            9862531.      61719654.       96864780.
    ## 6 1980           15295860.      45448725.       67233628.
    ## 7 1990           33615957.      48464764.       94201941.
    ## 8 2000           34294125.      41866597.       82951630.
    ## 9 2010           37867277.      47552408.      115140780.

``` r
movies_data %>%
  mutate(year = year(release_date)) %>% 
  dplyr::filter(!str_detect(movie, "Wonder Park"),  # remove the outiler
                !year < 1988) %>%   
  group_by(genre, year) %>%
  summarize(worldwide_gross = median(worldwide_gross, na.rm = T)) %>%
  ggplot(aes(year, worldwide_gross, color = genre)) +
  geom_line(size = .76) +
  scale_color_manual(values = c("darkorange", "cyan4", "black", "green", "red")) +
  scale_y_continuous(labels = dollar_format()) +
  scale_x_continuous(breaks = seq.int(1988, 2018, 3)) +
  theme(legend.position = "top",
        legend.title = element_blank(),
        plot.title = element_text(size = 20,
                                  face = "bold",
                                  hjust = .5),
        plot.subtitle = element_text(size = 14,
                                     face = "italic",
                                     hjust = .5)) +
  labs(x = "", y = "",
       title = "Typical Production Budget over time",
       subtitle = "Median budget by year, 1988-2018")
```

![](CODE_week30_files/figure-markdown_github/unnamed-chunk-14-1.png)

Adventure and Action movies show the highest budgets, Drama movies the lowest

### Worldwide profit ratio

Here wenotice something wth the previous time span, so we zoom in and consider the 2000-2018 horizon

``` r
movies_data %>%
  mutate(year = year(release_date)) %>% 
  dplyr::filter(!str_detect(movie, "Wonder Park"),  # remove the outiler
                !year < 1988) %>%   
  group_by(genre, year) %>%
  summarize(worldwide_profit = median(worldwide_profit, na.rm = T)) %>%
  ggplot(aes(year, worldwide_profit, color = genre)) +
  geom_line(size = .86) +
  #geom_point() +
  ylim(0,20) +
  scale_color_manual(values = c("darkorange", "cyan4", "black", "green", "red")) +
  scale_x_continuous(breaks = seq.int(1988, 2018, 3)) +
  labs(title = "Typical Movie profit by genre, 1988-2010", 
       subtitle = "Horror movies have the highest profit ratio ", x = "", y = "") +
  theme(legend.position = "top",
        legend.title = element_blank(),
        plot.title = element_text(size = 20,
                                  face = "bold",
                                  hjust = .5),
        plot.subtitle = element_text(size =14,
                                     face = "italic",
                                     hjust = .5))
```

![](CODE_week30_files/figure-markdown_github/unnamed-chunk-15-1.png)
