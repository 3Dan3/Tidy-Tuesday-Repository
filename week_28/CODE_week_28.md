Untitled
================
DanielH
October 9, 2018

-   [data processing](#data-processing)
-   [data exploration and plotting](#data-exploration-and-plotting)
-   [dumbbell](#dumbbell)

``` r
# load packages
library(tidyverse)
library(purrrlyr)
library(lubridate)
library(ggrepel)
library(ggthemes)
library(ggridges)
library(ggalt)
library(scales)
library(gridExtra)
library(knitr)


# load data
dat_raw <-
  read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2018-10-09/voter_turnout.csv")
```

data processing
---------------

``` r
# clean dataset
dat <-
  dat_raw %>%
  select(2, 5:7) %>%
  dplyr::filter(!str_detect(state, "United")) %>%
  dmap_at(2, factor)

# only keep complete cases
dat <-
  dat %>%
  na.omit()

# add new vars 'turnout' and 'election'
dat <-
  dat %>%
  mutate(turnout = votes / eligible_voters,
         election = if_else(year %in% c(seq.int(1980, 2012, 4)), 
                      "presidential", "midterm")) %>%
  dmap_at(6, factor) %>% 
  select(year, state, election, everything())
   
# check
dat %>%
  sample_n(5)
```

    ## # A tibble: 5 x 6
    ##    year state    election       votes eligible_voters turnout
    ##   <int> <fct>    <fct>          <int>           <int>   <dbl>
    ## 1  2014 Florida  midterm      6026802        13914216   0.433
    ## 2  1998 Idaho    midterm       386720          843914   0.458
    ## 3  2000 Nebraska presidential  707223         1224178   0.578
    ## 4  1990 Nebraska midterm       604195         1131746   0.534
    ## 5  1998 Wyoming  midterm       178401          350136   0.510

data exploration and plotting
-----------------------------

#### average turnout

check the united states totals, the lousiana thing and so on

First we create a modified dataframe dat\_2

``` r
# date range
dat %>% 
  pull(year) %>%
  range() %>% 
  set_names(c("first", "last"))
```

    ## first  last 
    ##  1980  2014

``` r
# average turnout
dat %>%
  group_by(year, state, election) %>%
  summarize(avg_turnout = mean(turnout, na.rm = T)) %>%
  mutate_at(4, round, 3) %>%
  arrange(avg_turnout) %>%
  arrange(desc(avg_turnout)) %>%
  head(5) %>%
  kable(caption = "Best 5 Election turnouts")
```

|  year| state     | election     |  avg\_turnout|
|-----:|:----------|:-------------|-------------:|
|  2004| Minnesota | presidential |         0.788|
|  2008| Minnesota | presidential |         0.781|
|  2012| Minnesota | presidential |         0.764|
|  2004| Wisconsin | presidential |         0.753|
|  2004| Maine     | presidential |         0.749|

``` r
dat %>%
  group_by(year, state, election) %>%
  summarize(avg_turnout = mean(turnout, na.rm = T)) %>%
  mutate_at(4, round, 3) %>% 
  arrange(avg_turnout) %>%
  arrange(desc(avg_turnout)) %>%
  tail(5) %>%
  kable(caption = "Worst 5 Election turnouts")
```

|  year| state                | election |  avg\_turnout|
|-----:|:---------------------|:---------|-------------:|
|  2014| New York             | midterm  |         0.290|
|  1986| District of Columbia | midterm  |         0.288|
|  2006| District of Columbia | midterm  |         0.287|
|  2014| Indiana              | midterm  |         0.287|
|  1998| Tennessee            | midterm  |         0.251|

``` r
# --------------------------------- plot turnout distribution by type of election

# outliers subset
neg_outliers <-
  dat %>%
  mutate(state = str_replace_all(state, 
                                 "District of Columbia", "DC")) %>%
  dplyr::filter(election == "presidential") %>%
  arrange(turnout) %>%
  head(2) 

pos_outliers <-
  dat %>%
  dplyr::filter(election == "presidential") %>%
  arrange(desc(turnout)) %>%
  head(1)

outliers_dat <-
  pos_outliers %>% 
  bind_rows(neg_outliers)


# plot turnout distribution by type of election
(plot_avg <-
  dat %>%
    ggplot(aes(election, turnout, fill = election)) +
    geom_boxplot( show.legend = F, outlier.size = 1.5,
                width = .5) +
    geom_text_repel(aes(label = paste(state, year, sep = ", ")),
                  size = 2.8, nudge_x = 1.5,
                  color = "black", data = outliers_dat, hjust = 1,
                  direction = "x", segment.colour = "grey",
                  segment.size = .15, show.legend = F) +
    scale_fill_manual(values = c("darkolivegreen", "chocolate")) +
    scale_y_continuous(labels = scales::percent) +
    labs(title = "US Elections turnout",
         subtitle = "percentage turnout by election type, 1980-2014",
         x = "", y = "") +
    theme_minimal()) +
    theme(plot.title = element_text(size = 20,
                                  family = "Times",
                                  face = "bold",
                                  color = "black",
                                  hjust = 0,
                                  lineheight = 1),
        plot.subtitle = element_text(size = 11,
                                     hjust = 0,
                                     face = "italic"))
```

<img src="CODE_week_28_files/figure-markdown_github/unnamed-chunk-3-1.png" style="display: block; margin: auto;" />

#### turnout over time

``` r
# 
dat %>%
group_by(year, election) %>%
  summarize(turnout = mean(turnout)) %>%
  ggplot(aes(year, turnout, color = election)) +
  geom_line(show.legend = F, size =.6) +
  geom_point(show.legend = F, size = 2) +
  scale_color_manual(values = c("darkolivegreen", "chocolate")) +
  scale_y_continuous(labels = scales::percent) +
  facet_wrap(~election) +
  theme_minimal() + 
  theme(plot.title = element_text(size = 20,
                                  family = "Times",
                                  face = "bold",
                                  color = "black",
                                  hjust = 0,
                                  lineheight = 1),
        plot.subtitle = element_text(size = 11,
                                     hjust = 0,
                                     face = "italic"),
        #strip.background = element_rect(fill = "gray95"),
        strip.text = element_text(size = 12,
                                  color = "black",
                                  face = "bold")) +
  labs(title = "US Elections turnout",
       subtitle = "percent average turnout by election type, 1980-2014",
       x = "", y ="")
```

<img src="CODE_week_28_files/figure-markdown_github/unnamed-chunk-4-1.png" style="display: block; margin: auto;" />

``` r
dat %>%
  dmap_at(3, as.character) %>% 
  ggplot(aes(year, turnout, color = election)) +
  geom_point(alpha = .75) +
  scale_color_manual(values = c("darkolivegreen", "chocolate")) +
  geom_smooth(se = FALSE, show.legend = F) +
  scale_y_continuous(labels = scales::percent,
                     limits = c(0.15, 0.8),
                     breaks = c(.2, .3, .4,.5,.6,.7,.8)) +
  theme_minimal() +
  theme(plot.title = element_text(size = 20,
                                  family = "Times",
                                  face = "bold",
                                  color = "black",
                                  hjust = 0,
                                  lineheight = 1),
        plot.subtitle = element_text(size = 11,
                                     hjust = 0,
                                     face = "italic"),
        legend.position = 'top',
        legend.title = element_blank()) +
  labs(title = "US Elections turnout",
       subtitle = "percentage turnout in each state, 1980-2014",
         x = "", y = "")
```

<img src="CODE_week_28_files/figure-markdown_github/unnamed-chunk-5-1.png" style="display: block; margin: auto;" />

``` r
dat %>% 
  ggplot(aes(turnout, fct_reorder(state, turnout), fill = election)) +
  geom_density_ridges(scale = 2, alpha = .7) +
  scale_fill_manual(values = c("darkolivegreen", "chocolate")) +
  theme_minimal() +
  scale_x_continuous(labels = scales::percent) +
  labs(title = "US Elections turnout",
       x = "", y = "")
```

    ## Picking joint bandwidth of 0.0209

<img src="CODE_week_28_files/figure-markdown_github/unnamed-chunk-6-1.png" style="display: block; margin: auto;" />

``` r
# scale_x_continuous(limits = c(25, 85))



dat %>%
  dplyr::filter(election == "presidential") %>% 
  ggplot(aes(turnout, fct_reorder(state, turnout), fill = election)) +
  geom_density_ridges(scale = 2, alpha = .7,
                      show.legend = F, fill = "darkolivegreen") +
  theme_minimal() +
  scale_x_continuous(labels = scales::percent) +
  labs(title = "US Elections turnout",
       x = "", y = "")
```

    ## Picking joint bandwidth of 0.0224

<img src="CODE_week_28_files/figure-markdown_github/unnamed-chunk-6-2.png" style="display: block; margin: auto;" />

``` r
dat %>%
  dplyr::filter(election == "midterm") %>% 
  ggplot(aes(turnout, fct_reorder(state, turnout), fill = election)) +
  geom_density_ridges(scale = 2, alpha = .7,
                      show.legend = F, fill = "chocolate") +
  theme_minimal() +
  scale_x_continuous(labels = scales::percent) +
  labs(title = "US Elections turnout",
       x = "", y = "")
```

    ## Picking joint bandwidth of 0.0195

<img src="CODE_week_28_files/figure-markdown_github/unnamed-chunk-6-3.png" style="display: block; margin: auto;" />

<https://twitter.com/CorbanNemeth/status/1050109201832271872>

<https://gitlab.com/snippets/1761797>

<https://twitter.com/search?f=tweets&vertical=default&q=%23TidyTuesday&src=savs>

<https://rud.is/b/2016/04/17/ggplot2-exercising-with-ggalt-dumbbells/>

<https://cran.r-project.org/web/packages/ggalt/vignettes/ggalt_examples.html>

<https://github.com/HaydenMacDonald/hmd-tidy-tuesday/blob/master/week28/week28.Rmd>

dumbbell
--------

``` r
# plot both election types avg turnout for each state
dat %>%
  group_by(state, election) %>%
  summarize(avg_turnout = mean(turnout, na.rm = TRUE)) %>%
  ggplot(aes(fct_reorder(state, avg_turnout), avg_turnout, color = election)) +
  geom_point() +
  geom_line(color = "black",
            size = .7, alpha = .96) +
  scale_color_tableau(palette = "Tableau 10") +
  scale_y_continuous(labels = scales::percent,
                     limits = c(0.3, 0.8),
                     breaks = c(.3, .4,.5,.6,.7,.8)) +
  coord_flip() +
  theme_minimal() +
  theme(plot.title = element_text(size = 25,
                                  family = "Times",
                                  face = "bold",
                                  color = "black",
                                  hjust = 0.15,
                                  lineheight = 1),
        plot.subtitle = element_text(size = 11,
                                     hjust = .1,
                                     face = "italic"),
        legend.text.align = 4,
        legend.position = 'top',
        legend.title = element_blank()) +
  labs(title = "US Elections average turnout",
       subtitle = "The highest turnout in Maine and the Upper Midwest",
       x = "", y ="")
```

![](CODE_week_28_files/figure-markdown_github/unnamed-chunk-7-1.png)

``` r
ggsave("US_average_turnout.png", width = 8, height = 8)
```

#### dumbbell\_practice

<https://rud.is/b/2016/04/17/ggplot2-exercising-with-ggalt-dumbbells/>

``` r
dat %>%
  group_by(state, election) %>%
  summarize(avg_turnout = mean(turnout, na.rm = TRUE)) %>%
  spread(election, avg_turnout) %>%
  mutate(mean_turnout = (presidential + midterm) / 2) %>%
  arrange(desc(mean_turnout)) %>% 
  ggplot(aes(fct_reorder(state, mean_turnout),
             x=midterm, xend=presidential), color = election) +
  geom_dumbbell(size = .76,  size_x = 1.3, size_xend = 1.3, 
                color = "darkgrey", 
                colour_x = "darkolivegreen", 
                colour_xend = "chocolate",
                show.legend = TRUE) +
  scale_x_continuous(labels = scales::percent,
                     limits = c(0.3, 0.8),
                     breaks = c(.3, .4,.5,.6,.7,.8)) +
  theme_minimal() +
  theme(plot.title = element_text(size = 21,
                                  family = "Times",
                                  face = "bold",
                                  color = "black",
                                  hjust = 0.15,
                                  lineheight = 1),
        axis.text = element_text(color = 'black', 
                                 size = 8,
                                 face = "italic"),
        plot.subtitle = element_text(size = 10,
                                     hjust = .1,
                                     face = "italic"),
        legend.text.align = 0,
        legend.position = 'top',
        legend.title = element_blank()) +
  labs(title = "US Elections average turnout",
       subtitle = "The highest turnout in Maine and the Upper Midwest",
       x = "", y ="")
```

![](CODE_week_28_files/figure-markdown_github/unnamed-chunk-8-1.png)

``` r
ggsave("dumbbell.png", width = 7, height = 7)  
```

#### lollipop

``` r
dat %>%
  dplyr::filter(election == "presidential") %>% 
  group_by(state) %>%
  summarize(avg_turnout = mean(turnout, na.rm = T)) %>%
  ggplot(aes(fct_reorder(state, avg_turnout), avg_turnout)) +
  geom_lollipop(point.colour="chocolate", size = .6,
                point.size=1.3, horizontal=FALSE) +
  coord_flip() +
  scale_y_continuous(labels = scales::percent,
                     breaks = c(.05, .15, .25, .35, .45, .55, .65, .75)) +
  theme_minimal() +
  theme(plot.title = element_text(size = 19,
                                  family = "Times",
                                  face = "bold",
                                  color = "black",
                                  hjust = 0,
                                  lineheight = 1),
        axis.text = element_text(color = 'black', 
                                 size = 8,
                                 face = "italic"),
        plot.subtitle = element_text(size = 10,
                                     hjust = .03,
                                     face = "italic"),
        legend.text.align = 0,
        legend.position = 'top',
        legend.title = element_blank()) +
  labs(title = "US presidential elections average turnout",
       x = "", y = "")
```

![](CODE_week_28_files/figure-markdown_github/unnamed-chunk-9-1.png)

``` r
ggsave("presidential_turnout.png", width = 7, height = 7)
```

``` r
dat %>%
  group_by(state, election) %>%
  summarize(avg_turnout = mean(turnout, na.rm = T)) %>%
  ggplot(aes(fct_reorder(state, avg_turnout), avg_turnout, color = election)) +
  geom_lollipop(point.colour="black", size = .8,
                point.size=1.5, horizontal=FALSE,
                show.legend = FALSE) +
  scale_color_manual(values = c("darkolivegreen", "chocolate")) +
  coord_flip() +
  scale_y_continuous(labels = scales::percent,
                     breaks = c(.05, .15, .25, .35, .45, .55, .65, .75)) +
  theme_minimal() +
  facet_wrap(~election) +
  labs(title = "US presidential elections average turnout",
       x = "", y = "") +
  theme(plot.title = element_text(size = 18,
                                  family = "Times",
                                  face = "bold",
                                  color = "black",
                                  hjust = 0.3,
                                  lineheight = 1),
        axis.text = element_text(color = 'black', 
                                 size = 8,
                                 face = "plain"),
        strip.text = element_text(size = 10,
                                  color = "black",
                                  face = "bold"),
        plot.subtitle = element_text(size = 10,
                                     hjust = .03,
                                     face = "italic"),
        legend.text.align = 0,
        legend.position = 'top',
        legend.title = element_blank()) 
```

![](CODE_week_28_files/figure-markdown_github/unnamed-chunk-10-1.png)

``` r
ggsave("average__turnout.png", width = 8, height = 8)
```
