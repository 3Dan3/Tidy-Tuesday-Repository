week 19
================
DanielH
August 7, 2018

-   [exploratory data analysis](#exploratory-data-analysis)
-   [industry trend](#industry-trend)

``` r
# load packages
library(tidyverse)
library(purrrlyr)
library(wesanderson)

# read data
airline_safety_dat <-
  read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2018-08-07/week19_airline_safety.csv")



# data manipulation
airline_safety_dat <-
  airline_safety_dat %>%
  select(-1) %>%
  dmap_at(c(1, 3, 4), factor)

# create list with elements = levels of `type_of_event`
airline_safety_list <-
  airline_safety_dat %>% 
  split(.$type_of_event) %>%
  as.list()

# extract vector to name list elements
names_vector <-
  airline_safety_dat %>% 
  pull(type_of_event) %>%
  unique() %>%
  as.character() %>%
  sort()

# name list elements
airline_safety_list <-
  airline_safety_list %>%
  set_names(names_vector)
```

### exploratory data analysis

Here we want to create some plots to get a sense of our dataset. We want to create plots showing the variation in terms of fatalities/incidents/fatal accidents between periods 1985-99 and 2000-14.

First, we create a couple of custom functions:

-   `wrangl_funct`: for the required data manipulation
-   `plot_funct`: for plotting

We then map these functions to our list `airline_safety_list` we created at the previous step.

For the mapping we use functions from the `purrr` package.

``` r
# -------------------------------------- Custom Functions ---------------------------------------

# define wrangling function
wrangl_funct <- function(df) {
  
  df %>%
    mutate(year_range = if_else(year_range == "00_14", 
                                "second", "first")) %>%
    spread(year_range, n_events) %>%
    mutate(diff_fatal = second - first,
           type_of_event = str_replace_all(type_of_event, "_", " ")) %>%
    arrange(desc(-diff_fatal)) %>%
    select(1, 3, 6)
  
}


# define plot function
plot_funct <- function(df) {
  
  df %>%
    ggplot(aes(fct_reorder(airline, -diff_fatal), diff_fatal)) +
    geom_col(aes(fill = diff_fatal < 0), 
             color = "white") +
    scale_fill_manual(guide = FALSE, breaks = c(TRUE, FALSE), 
                    values=c("red", "cadetblue")) +
    theme_minimal() +
    theme(plot.title = element_text(size = 20,
                                  family = "Times",
                                  face = "bold",
                                  color = "black",
                                  hjust = 0,
                                  lineheight = 1),
        plot.subtitle = element_text(size = 13,
                                     hjust = 0,
                                     face = "italic")) +
    coord_flip() +
    ggtitle(paste("Variation in", unique(df$type_of_event), 
                   "between \n1985-99 and 2000-14")) +
    labs(subtitle = "Blue: improved safety, Red: worsened safety", 
         x = "",
         y ="")
    
}
```

Now we can map the custom functions `plot_funct` and `wrangl_funct` to our list

``` r
# call functions, create and save plots
airline_safety_list %>%
  modify_depth(1, wrangl_funct) %>%       # manipulation of the 3 elements in the list
  modify_depth(1, plot_funct) %>%          # plot each element of the list
  walk2(paste0(names_vector, ".png"),        # save plots to our directory 
        ., ggsave, width = 7, height = 8)
```

### industry trend

We now look at the overall trend between the two periods for each type of event

``` r
airline_safety_dat %>%
  mutate(year_range = str_replace(year_range, "00_14", "2000-2014"),
         year_range = str_replace_all(year_range, "85_99", "1985-1999"),
         year_range = factor(year_range, levels = c("1985-1999","2000-2014"),
                             labels = c("1985-1999","2000-2014")),
         type_of_event = str_replace_all(type_of_event, 
                                         "fatal_accidents", "fatal accidents")) %>% 
  group_by(year_range) %>%
  ggplot(aes(year_range, n_events, fill = type_of_event)) +
  geom_boxplot(show.legend = FALSE, alpha = .76) +
  facet_wrap(~type_of_event, scales = "free") +
  labs(title = "Airline Safety, 1985-1999 vs 2000-2014",
       subtitle = "Safety has improved for all type of events",
       x = "", y = "") +
  scale_fill_manual(values = wes_palette("Moonrise2")) +
  theme_minimal() +
  theme(plot.title = element_text(size = 24,
                                  family = "Times",
                                  face = "bold",
                                  color = "black",
                                  hjust = 0.5,
                                  lineheight = 1),
        plot.subtitle = element_text(size = 19,
                                     hjust = 0.5,
                                     face = "italic"),
        strip.text = element_text(size = 17.5,
                                  face = "plain"),
        strip.text.x = element_text(size = 17.5),
        strip.background = element_rect(colour="white", 
                                        fill="grey94"))
```

![](CODE_week_19_files/figure-markdown_github/unnamed-chunk-4-1.png)

``` r
ggsave(filename = "industry_trend.png", width = 10, height = 7)
```
