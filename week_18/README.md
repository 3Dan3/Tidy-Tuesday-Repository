# Tidy Tuesday week 18

---

This week's data is from the Dallas Open Data website https://www.dallasopendata.com/

---

* __The City of Dallas, through its services, works to keep the city safe, compassionate, and healthy for animals as well as for people.__

* __The department that responds to animal incidents throughout the city is the Dallas Animal Services, DAS.__

* __Our dataset start date is October 01, 2016__

---

__data source__: https://github.com/rfordatascience/tidytuesday/blob/master/data/week18_dallas_animals.xlsx
