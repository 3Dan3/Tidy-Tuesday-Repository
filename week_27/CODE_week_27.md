Untitled
================
DanielH
October 4, 2018

-   [data wrangling](#data-wrangling)
-   [plots](#plots)

------------------------------------------------------------------------

data wrangling
--------------

``` r
# load packages
library(tidyverse)
library(purrrlyr)
library(lubridate)
library(ggrepel)
library(gridExtra)

# read data
dat_raw <-
  read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2018-10-02/us_births_2000-2014.csv")


# wrangling

day_labels <- c("Monday", "Tuesday", "Wednesday","Thursday", 
                "Friday", "Saturday", "Sunday")

dat <-
  dat_raw %>%
  arrange(month) %>% 
  mutate(month = factor(month,  
                        labels = month.name %>%
                          str_sub(1,3)),
         day_of_week = factor(day_of_week, 
                              labels = day_labels %>%
                                str_sub(1,3))) %>%
  dmap_at(c(1:4), factor)
```

plots
-----

``` r
# -------------------------------- plot births by month 
plot1 <-
  dat_raw %>%
  group_by(month) %>%
  summarize(total_births = sum(births, na.rm = TRUE)) %>%
  ggplot( aes(month, total_births/1000000)) +
  geom_point(color = "black") +
  geom_line(size = .75, color = "steelblue3",
            alpha = .4) +
  ylim(4.5, 5.66) +
  scale_x_discrete(limits = month.abb) +
  theme_minimal() +
  labs(title = "",
       subtitle = "",
       x = "", y = "") +
  theme(plot.title = element_text(size = 12.5,
                                  family = "Times New Roman",
                                  face = "bold",
                                  color = "black",
                                  hjust = 0,
                                  lineheight = 1),
        plot.subtitle = element_text(size = 9,
                                     hjust = 0.02,
                                     face = "italic"))


plot2 <-
  dat_raw %>%
  group_by(month) %>%
  summarize(total_births = sum(births, na.rm = TRUE)) %>%
  ggplot(aes(month, total_births/1000000)) +
  geom_col(stat = "identity", fill = "steelblue3",
           alpha = .4) +
  scale_y_discrete(limits = c(0,1,2,3,4,5,6)) +
  scale_x_discrete(limits = month.abb) +
  theme_minimal() +
  labs(title = "Births in the US peak during summer",
       subtitle = "US total births by month in milions",
       x = "", y = "") +
  theme(plot.title = element_text(size = 18,
                                  family = "Times New Roman",
                                  face = "bold",
                                  color = "black",
                                  hjust = 0.5,
                                  lineheight = 1),
        plot.subtitle = element_text(size = 11,
                                     hjust = 0.52,
                                     face = "italic"))


# plots together
grid.arrange(plot2, plot1) %>%
  ggsave(filename = "by_month.png", width = 6, height = 6)
```

<img src="CODE_week_27_files/figure-markdown_github/unnamed-chunk-2-1.png" style="display: block; margin: auto;" />

``` r
# ----------------------------- plot births by year

sub_data <-
  dat_raw %>%
  arrange(month) %>% 
  mutate(month = factor(month,  
                        labels = month.name %>%
                          str_sub(1,3)),
         day_of_week = factor(day_of_week, 
                              labels = day_labels %>%
                                str_sub(1,3))) %>%
  group_by(year)  %>% 
  summarise(tot_births = sum(births, na.rm = T)) %>%
  mutate(tot_births = tot_births / 1000000) %>% 
  arrange(desc(tot_births)) %>%
  head(3)



plotA <-
  dat_raw %>%
  arrange(month) %>% 
  mutate(month = factor(month,  
                        labels = month.name %>%
                          str_sub(1,3)),
         day_of_week = factor(day_of_week, 
                              labels = day_labels %>%
                                str_sub(1,3))) %>%
  group_by(year)  %>% 
  summarise(tot_births = sum(births, na.rm = T)) %>%
  mutate(tot_births = tot_births / 1000000) %>%
  ggplot(aes(year, tot_births)) +
  geom_line( size = .5,
             color = "darkslategray") +
  geom_point(size = 1.4, color = "darkslategray") +
  geom_point(color=" red", data = sub_data,
             size = 1.6) +
  geom_label_repel(aes(label = year),
                 size = 3, data = sub_data,
                 nudge_y = .075, color = "red") +
  ylim(3.8, 4.6) +
  xlim(2000, 2015) +
  theme_minimal() +
  theme(plot.title = element_text(size = 18,
                                  family = "Times New Roman",
                                  face = "bold",
                                  color = "black",
                                  hjust = 0.5,
                                  lineheight = 1),
        plot.subtitle = element_text(size = 11,
                                     hjust = 0.51,
                                     face = "italic")) +
  labs(y = "", x = "")



plotB <-
  dat_raw %>%
  group_by(year)  %>% 
  summarise(tot_births = sum(births, na.rm = T)) %>%
  ggplot(aes(year, tot_births/1000000)) +
  geom_col(fill = "darkslategray", alpha = .7) +
  xlim(2000, 2015) +
  ylim(0, 4.8) +
  labs(title = "US Births peaked during years 2006-2008",
       subtitle = "total US births in milions, years 2000-2014",
       x = "", y = "") +
  theme_minimal() +
  theme(plot.title = element_text(size = 18,
                                  family = "Times New Roman",
                                  face = "bold",
                                  color = "black",
                                  hjust = 0.5,
                                  lineheight = 1),
        plot.subtitle = element_text(size = 11,
                                     hjust = 0.51,
                                     face = "italic"))


grid.arrange(plotB, plotA) %>%
  ggsave(filename = "by_year.png", width = 6, height = 6)
```

<img src="CODE_week_27_files/figure-markdown_github/unnamed-chunk-2-2.png" style="display: block; margin: auto;" />

``` r
# ------------------------------ plot births by day of the week

dat_raw %>%
  group_by(day_of_week) %>%
  summarize(total_births = sum(births, na.rm = TRUE)) %>%
  ggplot(aes(day_of_week, total_births/1000000)) +
  geom_col(size = .65, fill = "darkslategray",
           alpha = .5) +
  scale_x_discrete(limits = day_labels %>% str_sub(1,3)) +
  theme_minimal() +
  labs(x = "", y = "",
       title = "Weekends have the lowest births",
       subtitle = "US total births by day of the week, millions") +
  theme(plot.title = element_text(size = 15,
                                  family = "Times New Roman",
                                  face = "bold",
                                  color = "black",
                                  hjust = 0,
                                  lineheight = 1),
        plot.subtitle = element_text(size = 10,
                                     hjust = 0.02,
                                     face = "italic"))
```

<img src="CODE_week_27_files/figure-markdown_github/unnamed-chunk-3-1.png" style="display: block; margin: auto;" />

``` r
ggsave(filename = "by_weekday.png", width = 4, height = 3)
```

``` r
dat %>%
  group_by(date_of_month) %>%
  summarize(tot_births = sum(births, na.rm = T)) %>%
  ggplot(aes(date_of_month, tot_births/1000000)) +
  geom_point() +
  geom_line(group = 1, color = "firebrick1") +
  theme_minimal() +
  ylim(1, 2.2) +
  labs(title = "Less Babies by the end of the Month",
       subtitle = "days of the month by total births in milions",
       x = "", y = "") +
  theme(plot.title = element_text(size = 18,
                                  family = "Times New Roman",
                                  face = "bold",
                                  color = "black",
                                  hjust = 0.5,
                                  lineheight = 1),
        plot.subtitle = element_text(size = 11,
                                     hjust = 0.51,
                                     face = "italic"))
```

<img src="CODE_week_27_files/figure-markdown_github/unnamed-chunk-4-1.png" style="display: block; margin: auto;" />

``` r
ggsave(filename = "by_day_of_month.png", width = 7, height = 3)
```
