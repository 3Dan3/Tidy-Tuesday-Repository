Untitled
================
DanielH
October 21, 2018

-   [intro](#intro)
-   [data processing](#data-processing)
-   [data exploration](#data-exploration)
-   [highest earning categories of majors](#highest-earning-categories-of-majors)
-   [highest earning majors](#highest-earning-majors)
-   [how does gender breakdown relate to typical earnings?](#how-does-gender-breakdown-relate-to-typical-earnings)
-   [plots](#plots)

intro
-----

This weeks case is about college majors. Our data is taken from the fivethirtyeight website.

We take a look at at the vars in the dataset

    ##  [1] "Rank"                 "Major_code"           "Major"               
    ##  [4] "Total"                "Men"                  "Women"               
    ##  [7] "Major_category"       "ShareWomen"           "Sample_size"         
    ## [10] "Employed"             "Full_time"            "Part_time"           
    ## [13] "Full_time_year_round" "Unemployed"           "Unemployment_rate"   
    ## [16] "Median"               "P25th"                "P75th"               
    ## [19] "College_jobs"         "Non_college_jobs"     "Low_wage_jobs"

data processing
---------------

-   add a new var `prop_men`, the proportion of men and rename `ShareWomen` to `prop_women` of course
-   select relevnt variables, reformat col names to lower and character vectors to title

``` r
col_majors_dat2 <-
  col_majors_dat_raw %>%
  mutate(ShareMen = 1- ShareWomen) %>%
  mutate_if(is_character, str_to_title)

col_majors_dat2 <-
  col_majors_dat2 %>%
  select(3,7,8,22, everything()) %>%
  select(-(Rank:Unemployed), -(College_jobs:Low_wage_jobs)) %>%
  rename(prop_women = ShareWomen,
         prop_men = ShareMen)

colnames(col_majors_dat2) <- 
  col_majors_dat2 %>%
  names() %>%
  str_to_lower() 
```

data exploration
----------------

Let's take a look at the median salary distribution for all majors

### median salary

``` r
plot_hist <-
  col_majors_dat2 %>%
  ggplot(aes(median)) +
  geom_histogram(color = "white") +
  scale_x_continuous(labels = dollar) +
  labs(y = "")



plot_box <-
  col_majors_dat2 %>%
  ggplot(aes("", median)) +
  geom_boxplot(width = .36, fill = "steelblue2",
               alpha = .7) +
  scale_y_continuous(labels = dollar) +
  coord_flip() +
  labs(x = "", y = "")

grid.arrange(plot_hist, plot_box, nrow = 1)
```

![](CODE_week_29_files/figure-markdown_github/unnamed-chunk-3-1.png)

We see that there are some positive outliers.

Next, we look at the median salary by major category.

we rank the major types by median salary and annotate the 3 majors with the highest median starting salary.

``` r
# find outliers
outliers_dat <-
  col_majors_dat2 %>%
  dplyr::filter(major_category %in% c("Engineering", "Business", "Physical Sciences")) %>% 
  group_by(major_category) %>%
  arrange(desc(median)) %>% 
  top_n(1)
```

    ## Selecting by p75th

``` r
# plot
col_majors_dat2 %>%
  ggplot(aes(fct_reorder(major_category, median), median, fill = major_category)) +
  geom_boxplot(show.legend = F, outlier.colour = "black") +
  #geom_text_repel(aes(label = major),
                  #size = 2.6, data = outliers_dat,  show.legend = F) +
  geom_text_repel(aes(label = major),
                  size         = 2.7,
                  nudge_x      = -0.2,
                  direction    = "y",
                  angle        = 0,
                  vjust        = 0,
                  segment.size = 0.4,
                  data = outliers_dat,
                  color = "darkgreen",
                  show.legend = F) +
  scale_y_continuous(labels = dollar) +
  theme_fivethirtyeight() +
  coord_flip() +
  labs(title = "Median Starting Salary by Major Type",
       x = "", y = "")
```

![](CODE_week_29_files/figure-markdown_github/unnamed-chunk-4-1.png)

As we can see STEM and Business fields tend to have a higher starting salary. In green we can se the top 3 majors, which also happen to be outliers within their field.

Next, we want

<https://www.youtube.com/watch?time_continue=1296&v=nx5yhXAQLxw>

<https://twitter.com/jsonbaik/status/1054442723653140480>

------------------------------------------------------------------------

highest earning categories of majors
------------------------------------

``` r
col_majors_dat2 %>%
  group_by(major_category) %>%
  summarize_at(c(6:8), mean, na.rm = TRUE) %>% 
  ggplot(aes(fct_reorder(major_category, median), x =  p25th, xend = p75th)) +
  geom_dumbbell(color = "black", colour_x = "firebrick1", 
                colour_xend = "steelblue", size_x = 1.3,
                size_xend = 1.3, size = .7) +
  scale_x_continuous(labels = dollar) +
  theme_minimal() +
  labs(title = "Salary Range by category of Major", subtitle = "STEM degrees command higher avg salaries, but the IQR is wide, \nEducation and liberal arts have lower salaries and narrower range",
       x = "", y = "")
```

![](CODE_week_29_files/figure-markdown_github/unnamed-chunk-5-1.png)

highest earning majors
----------------------

``` r
col_majors_dat2 %>%
  arrange(desc(median)) %>%
  head(20) %>% 
  ggplot(aes(fct_reorder(major, median), median, color = major_category)) +
  geom_lollipop(size = .85,
                point.size=2.0, horizontal=FALSE,
                show.legend = T) +
  scale_color_manual(values = c("red", "black",
                     "orange", "cyan3")) +
  #scale_color_tableau(palette = "Tableau 10") +
  scale_y_continuous(labels = dollar, limits = c(0, 115000)) +
  coord_flip() +
  theme_minimal() +
  
  labs(title = "Highest paying majors",
       subtitle = "Median Salary...",
       x = "", y = "")
```

![](CODE_week_29_files/figure-markdown_github/unnamed-chunk-6-1.png)

``` r
col_majors_dat2 %>%
  group_by(major) %>%
  summarize_at(c(6:8), mean, na.rm = TRUE) %>%
  arrange(desc(median, p75th)) %>%
  head(20) %>% 
  ggplot(aes(fct_reorder(major, median), x =  p25th, xend = p75th)) +
  geom_dumbbell(color = "cornflowerblue",  size_x = 1.3,
                size_xend = 1.3, size = .7) +
  geom_point(aes(median, major), color = "red",
             size = 1.25, alpha = .6) +
  scale_x_continuous(labels = dollar) +
  theme_minimal() +
  labs(title = "Salary Range by Major", subtitle = "Interquartile Range for top 20 Majors by Salary. Median: red",
       x = "", y = "")
```

![](CODE_week_29_files/figure-markdown_github/unnamed-chunk-7-1.png)

how does gender breakdown relate to typical earnings?
-----------------------------------------------------

------------------------------------------------------------------------

------------------------------------------------------------------------

plots
-----

``` r
# gather gender cols
col_majors_dat <-
  col_majors_dat_raw %>%
  gather("Gender", "Students", Men:Women) %>%   # long format for M & W cols
  select(Major, Major_category, Gender, Students, Total, everything(), -c(Major_code, Rank)) %>%
  rename(tot_students = Total) %>%
  mutate_at("Gender", str_to_lower)

# col names to lower case
colnames(col_majors_dat) <-
  col_majors_dat %>%
  names() %>%
  str_to_lower()


# check
col_majors_dat %>%
  sample_n(5)
```

    ## # A tibble: 5 x 19
    ##   major major_category gender students tot_students sharewomen sample_size
    ##   <chr> <chr>          <chr>     <int>        <int>      <dbl>       <int>
    ## 1 NUTR~ Health         women     16346        18909      0.864         118
    ## 2 HEAL~ Health         women     13843        18109      0.764         184
    ## 3 EDUC~ Psychology & ~ women      2332         2854      0.817           7
    ## 4 MICR~ Biology & Lif~ men        6383        15232      0.581          62
    ## 5 ENGI~ Engineering    men        3526         4321      0.184          30
    ## # ... with 12 more variables: employed <int>, full_time <int>,
    ## #   part_time <int>, full_time_year_round <int>, unemployed <int>,
    ## #   unemployment_rate <dbl>, median <int>, p25th <int>, p75th <int>,
    ## #   college_jobs <int>, non_college_jobs <int>, low_wage_jobs <int>

``` r
# data manipulation
col_majors_dat3 <-
  col_majors_dat_raw %>%
  select(-c(Rank:Major_code, Employed:Unemployment_rate, P25th:Low_wage_jobs)) %>%
  select(Major, Major_category, everything()) %>% 
  dmap_at(1, str_to_title) %>%
  mutate(share_men = 1 - ShareWomen) %>%
  rename(Share_women = ShareWomen,
         share_men = share_men) %>%
  select(Major:Share_women, share_men, everything())

colnames(col_majors_dat3) <-
  col_majors_dat3 %>%
  names() %>%
  str_to_lower()

# check
col_majors_dat3 %>%
  names()
```

    ## [1] "major"          "major_category" "total"          "men"           
    ## [5] "women"          "share_women"    "share_men"      "sample_size"   
    ## [9] "median"

``` r
# plot gender breakdown by category of major
col_majors_dat3 %>%
  gather("gender", "students", men:women) %>% 
  ggplot(aes(fct_reorder(major_category, median), students, fill = gender)) +
  geom_bar(stat = "identity", position = "fill",
           alpha = 1) +
  scale_fill_manual(values = c("steelblue3","darkorange")) +
  scale_y_continuous(labels = percent) +
  coord_flip() +
  geom_hline(yintercept = .5, color = "white",
             size = 1.6) +
  theme_minimal() +
  theme(legend.position = "right",
        legend.title = element_blank()) +
  labs(title = "Major type Gender Breakdown ",
       subtitle = "Major types ordered by median salary",
       x = "", y = "")
```

![](CODE_week_29_files/figure-markdown_github/unnamed-chunk-10-1.png)

``` r
col_majors_dat3 %>%
  gather("gender", "students", men:women) %>%
  drop_na(students) %>% 
  arrange(desc(median)) %>%
  head(30) %>% 
  ggplot(aes(fct_reorder(major, median), students, fill = gender)) +
  geom_bar(stat = "identity", position = "fill",
           alpha = 1) +
  scale_fill_manual(values = c("steelblue3","darkorange")) +
  scale_y_continuous(labels = percent, breaks = c(seq.int(0,1, .1))) +
  coord_flip() +
  geom_hline(yintercept = .5, color = "white",
             size = 1.6) +
  theme_minimal() +
  theme(legend.position = "right",
        legend.title = element_blank()) +
  labs(title = "College Majors  Gender Breakdown ",
       subtitle = "Top 20 by median salary",
       x = "", y = "")  
```

![](CODE_week_29_files/figure-markdown_github/unnamed-chunk-10-2.png)

``` r
col_majors_dat3 %>%
  drop_na(median) %>% 
  gather("gender", "students", men:women)
```

    ## # A tibble: 346 x 9
    ##    major major_category total share_women share_men sample_size median
    ##    <chr> <chr>          <int>       <dbl>     <dbl>       <int>  <int>
    ##  1 Petr~ Engineering     2339       0.121     0.879          36 110000
    ##  2 Mini~ Engineering      756       0.102     0.898           7  75000
    ##  3 Meta~ Engineering      856       0.153     0.847           3  73000
    ##  4 Nava~ Engineering     1258       0.107     0.893          16  70000
    ##  5 Chem~ Engineering    32260       0.342     0.658         289  65000
    ##  6 Nucl~ Engineering     2573       0.145     0.855          17  65000
    ##  7 Actu~ Business        3777       0.441     0.559          51  62000
    ##  8 Astr~ Physical Scie~  1792       0.536     0.464          10  62000
    ##  9 Mech~ Engineering    91227       0.120     0.880        1029  60000
    ## 10 Elec~ Engineering    81527       0.196     0.804         631  60000
    ## # ... with 336 more rows, and 2 more variables: gender <chr>,
    ## #   students <int>
